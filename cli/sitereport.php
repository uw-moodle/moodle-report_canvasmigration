<?php

/**
 * Runs the canvas migration site report to output to screen as a CSV.  This can then be piped to an actual CSV file
 *
 * @author John Hoopes
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions


if (moodle_needs_upgrading()) {
    echo "Moodle upgrade pending, backup execution suspended.\n";
    exit(1);
}


$indexcontroller = new \report_canvasmigration\local\controllers\index();
$indexcontroller->setup_page();
$indexcontroller->set_action('clisitereport');
$indexcontroller->handle_request();
