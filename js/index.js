var canvasmigration = canvasmigration || {};

canvasmigration.init = function() {
    'use strict';

    var panels = document.getElementsByClassName('panel');

    for(var x = 0; x < panels.length; x++){
        panels[x].children[0].onclick = function(){
            var panelid = this.parentElement.getAttribute('id');
            canvasmigration.toggle_panel(panelid);
        }
    }
};

canvasmigration.toggle_panel = function(id){
    'use strict';

    // expand/contract the panel body as well as rotate the collapsed icon
    var panel = document.getElementById(id);
    var expandicon = document.getElementById(id + '_expandicon');

    if(panel.children[1].classList.contains('hidden')){
        panel.children[1].classList.remove('hidden');
        expandicon.classList.add('open');
    }else{
        panel.children[1].classList.add('hidden');
        expandicon.classList.remove('open');
    }
};

(function preload(){
    window.addEventListener('load', function(){canvasmigration.init();}, false);
}());