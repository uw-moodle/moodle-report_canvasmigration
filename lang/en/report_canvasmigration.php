<?php


// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language strings page
 *
 * @package    report
 * @subpackage canvasmigration
 * @author     John Hoopes <john.hoopes@wisc.edu>
 * @copyright  2015 University of Wisconsin - Madison
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */



$string['pluginname'] = 'Canvas migration report';

$string['canvas_migration_config'] = 'Configure canvas migration';
$string['configure_description'] = 'Please assign scores associated with each module.  Be sure to reference the scores outlined in the link below';
$string['config_scores_link'] = 'Scores Detail Document';

$string['activitytype'] = 'Activity type';
$string['score'] = 'Score';
$string['coursecontacts'] = 'Course contacts';
$string['coursesubject'] = 'Course subject / Type';
$string['subjectnumber'] = 'Course Number';
$string['noofoccurances'] = 'Number of occurrences';
$string['enrollment'] = 'Enrollment';
$string['term'] = 'Term';
$string['modules'] = 'Activity modules';
$string['questions'] = 'Question types';