<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace report_canvasmigration\local\forms;

defined('MOODLE_INTERNAL') || die();
global $CFG;
require_once($CFG->libdir . '/formslib.php');

/**
 * Moodle form for confirming question add and get the time for the question
 * to appear on the page
 *
 * @package     mod_activequiz
 * @author      John Hoopes <hoopes@wisc.edu>
 * @copyright   2014 University of Wisconsin - Madison
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class configscores extends \moodleform {


    function definition(){

        $mform = $this->_form;
        $scores = $this->_customdata;

        $activitymodules = \core_component::get_plugin_list('mod');

        $mform->addElement('header', 'modules', get_string('modules', 'report_canvasmigration'));

        foreach($activitymodules as $modname => $amodule){

            $mform->addElement('text', $modname, get_string('modulename', $modname));
            $mform->setType($modname, PARAM_INT);
            // if there is a score in the scores table add it as the default for the module
            if(!empty($scores[$modname])){
                $mform->setDefault($modname, $scores[$modname]);
            }

        }
        // add questions

        $mform->addElement('header', 'modules', get_string('questions', 'report_canvasmigration'));

        foreach ( \question_bank::get_creatable_qtypes() as $qtypename => $qtype) {

            $mform->addElement('text', $qtypename, get_string('pluginname', 'qtype_' . $qtypename));
            $mform->setType($qtypename, PARAM_INT);

        }

        $this->add_action_buttons();
    }
}