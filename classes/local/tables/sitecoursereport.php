<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace report_canvasmigration\local\tables;

defined('MOODLE_INTERNAL') || die();
global $CFG;
require_once($CFG->libdir . '/tablelib.php');

/**
 * Table lib subclass for showing the overall grades for a realtime quiz activity
 *
 * @package     report_canvasmigration
 * @author      John Hoopes <hoopes@wisc.edu>
 * @copyright   2015 University of Wisconsin - Madison
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class sitecoursereport extends \flexible_table implements \renderable {


    /**
     * Setup the table, i.e. table headers
     *
     */
    public function setup() {
        // Set var for is downloading
        $this->is_downloadable(true);
        $isdownloading = $this->is_downloading();

        $this->set_attribute('cellspacing', '0');

        $columns = array(
            'course'         => get_string('course'),
            'coursecontacts' => get_string('coursecontacts', 'report_canvasmigration'),
            'subject'        => get_string('coursesubject', 'report_canvasmigration'),
            'subnum'         => get_string('subjectnumber', 'report_canvasmigration'),
            'term'           => get_string('term', 'report_canvasmigration'),
            'enrollment'     => get_string('enrollment', 'report_canvasmigration'),
        );

        $activitymodules = \core_component::get_plugin_list('mod');
        foreach($activitymodules as $modname => $amodule){
            $columns[$modname] = get_string('pluginname', $modname);
        }

        $this->define_columns(array_keys($columns));
        $this->define_headers(array_values($columns));

        $this->sortable(true);
        $this->collapsible(true);

        $this->set_attribute('cellspacing', '0');
        $this->set_attribute('cellpadding', '2');
        $this->set_attribute('class', 'generaltable generalbox');
        $this->set_attribute('align', 'center');

        parent::setup();
    }


    /**
     * Sets the data to the table
     *
     */
    public function set_data($allcourses) {
        global $CFG, $OUTPUT, $DB;

        $download = $this->is_downloading();
        $activitymodules = \core_component::get_plugin_list('mod');

        foreach ($allcourses as $course) {

            //get the course contacts into a comma separated list
            $ccontactsstr = '';
            foreach($course->coursecontacts as $contact){
                if(strlen($ccontactsstr) > 0){
                    $ccontactsstr .= ',';
                }
                $ccontactsstr .= " " . $contact['username'];
            }

            // start building the row
            $row = array();
            $row[] = $course->fullname;
            $row[] = $ccontactsstr;
            $row[] = $course->subject;
            $row[] = $course->catnum;
            $row[] = $course->term;
            $row[] = $course->enrolledcount;


            foreach($activitymodules as $modname => $amodule){

                $found = false;
                foreach($course->coursemods as $coursemod){
                    if($coursemod->modname = $modname){
                        $row[] = $coursemod->score;
                        $found = true;
                        break;
                    }
                }

                if(!$found){
                    $row[] = 0;
                }
            }
            $this->add_data($row);
        }
        return;
    }


}