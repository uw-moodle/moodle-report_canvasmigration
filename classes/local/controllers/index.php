<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace report_canvasmigration\local\controllers;

defined('MOODLE_INTERNAL') || die();
global $CFG;
require_once($CFG->libdir . '/coursecatlib.php');

/**
 * Index controller class
 *
 * @package   report_canvasmigration\local\controllers
 * @author    John Hoopes <john.hoopes@wisc.edu>
 * @copyright 2015 University of Wisconsin - Madison
 */
class index{

    /** @var  \stdClass $course  The course object from the DB */
    protected $course;

    /** @var  \context_course $context The context for the course secified, otherwise site course context */
    protected $context;

    /** @var  \moodle_url $pageurl The current page url */
    protected $pageurl;

    /** @var string $action The action for the particular request */
    protected $action;

    /** @var  \report_canvasmigration_renderer */
    protected $renderer;

    /**
     * Sets up the page with the required variables
     *
     * @param string $baseurl
     * @throws \moodle_exception Throws exception on setup error
     */
    public function setup_page($baseurl = NULL) {
        global $DB, $PAGE;

        $courseid = optional_param('course', '', PARAM_INT);

        if($courseid !== ''){
            $this->course = $DB->get_record('course', array('id' => $courseid));
        }else{
            $courseid = SITEID;
            $this->course = $DB->get_record('course', array('id' => $courseid));
        }

        if(empty($this->course)){
            throw new \moodle_exception('Invalid courseid');
        }

        $this->context = \context_course::instance($this->course->id);

        /** The following is only needed for web requests and not cli requests */
        if(CLI_SCRIPT === false){
            require_login($this->course);

            $this->pageurl = new \moodle_url($baseurl);
            $this->pageurl->remove_all_params();
            $this->pageurl->param('course', $courseid);
            $this->pageurl->param('action', $this->action);

            $PAGE->set_pagelayout('report');
            $PAGE->set_context($this->context);
            $PAGE->set_url($this->pageurl);
            $PAGE->set_title(strip_tags($this->course->shortname . ': ' . get_string("pluginname", "report_canvasmigration")));
            $PAGE->set_heading($this->course->fullname);
        }


        $this->renderer = $PAGE->get_renderer('report_canvasmigration');

    }

    /**
     *
     * Handles the request
     *
     */
    public function handle_request() {
        global $USER, $DB;

        switch($this->action){
            case 'clisitereport':

                $sitereportfactory = new \report_canvasmigration\local\sitereport_factory();
                $data = $sitereportfactory->get_data();

                $this->renderer->print_csv($data);


                break;
            default: // TODO: This case needs to be updated to use more of the factory method used above

                if($this->course->id === SITEID){
                    if(!is_siteadmin($USER)){
                        $this->renderer->setMessage('error', 'You are not allowed to see the site report');

                        $this->renderer->index_header();
                        $this->renderer->index_footer();
                    }

                    // go through all courses for the site report.
                    $allcourses = $DB->get_records('course');
                    foreach($allcourses as $course){

                        list($coursemods, $coursescore) = $this->get_mod_info($course);
                        $course->coursemods = $coursemods;
                        $course->coursescore = $coursescore;

                        if($coursemap = $DB->get_record('enrol_wisc_coursemap', array('courseid' => $course->id))){
                            $course->subject = $coursemap->subject;
                            $course->catnum = $coursemap->catalog_number;
                            $course->term = $coursemap->term;
                        }else{
                            $course->subject = '';
                            $course->catnum = '';
                            $course->term = '';
                        }
                        $coursecontext = \context_course::instance($course->id);
                        $course->enrolledcount = count_enrolled_users($coursecontext, '', 0, true);
                        $this->get_course_contacts($course);
                    }

                    $this->renderer->index_header();
                    $this->renderer->render_site_report($allcourses, $this->pageurl);
                    $this->renderer->index_footer();

                }else{ // normal course

                    list($coursemods, $totalscore) = $this->get_mod_info();

                    $this->renderer->index_header();
                    $this->renderer->render_course_report($coursemods, $totalscore, $this->course);
                    $this->renderer->index_footer();
                }




                break;
        }


    }

    /**
     * override the action for the class.  This should be called before handle_request.
     *
     * @param $action
     * @return $this
     */
    public function set_action($action){
        $this->action = $action;

        return $this;
    }

    /**
     * @param \stdClass $course The specified course to get mod info.  If empty, it will use $this->course
     * @return array
     */
    protected function get_mod_info($course = null){
        global $DB;

        if(is_null($course)){
            $course = $this->course;
        }

        // set up variables to score this course
        $coursemods = get_course_mods($course->id);
        $totalscore = 0;

        $configs = $DB->get_records('report_canvasmigrationconfig');
        $scores = array();
        foreach($configs as $config){
            $scores[$config->scoringitem] = $config->score;
        }
        // loop through the course mods and add a score to them
        foreach($coursemods as $coursemod){

            if(isset($scores[$coursemod->modname])){
                $coursemod->score = $scores[$coursemod->modname];
                $score = $scores[$coursemod->modname];
            }else{ // default for now to 0
                $coursemod->score = 0;
                $score = 0;
            }
            $totalscore += $score;

            // get the instance name
            $modinstance = $DB->get_record($coursemod->modname, array('id' => $coursemod->instance));
            $coursemod->modinstance = $modinstance;
        }

        return array($coursemods, $totalscore);

    }

    protected function get_course_contacts(&$course){

        $coursecontacts = array();

        $context = \context_course::instance($course->id);

        // Preload course contacts from DB.
        $courses = array($course->id => &$course);
        \coursecat::preload_course_contacts($courses);

        // Build return array with full roles names (for this course context) and users names.
        $canviewfullnames = has_capability('moodle/site:viewfullnames', $context);
        foreach ($course->managers as $ruser) {

            $user = new \stdClass();
            $user = username_load_fields_from_object($user, $ruser, null, array('id', 'username'));
            $role = new \stdClass();
            $role->id = $ruser->roleid;
            $role->name = $ruser->rolename;
            $role->shortname = $ruser->roleshortname;
            $role->coursealias = $ruser->rolecoursealias;

            $coursecontacts[$user->id] = array(
                'user' => $user,
                'role' => $role,
                'rolename' => role_get_name($role, $context, ROLENAME_ALIAS),
                'username' => fullname($user, $canviewfullnames)
            );
        }

        $course->coursecontacts = $coursecontacts;
    }




}