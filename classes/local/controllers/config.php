<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace report_canvasmigration\local\controllers;

use report_canvasmigration\local\forms\configscores;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/adminlib.php');

/**
 * Config controller class
 *
 * @package   report_canvasmigration\local\controllers
 * @author    John Hoopes <john.hoopes@wisc.edu>
 * @copyright 2015 University of Wisconsin - Madison
 */
class config{

    /** @var  string $action The action to take for the current request */
    protected $action;

    /** @var \moodle_url $pageurl */
    protected $pageurl;

    /** @var  \report_canvasmigration_renderer $renderer */
    protected $renderer;

    /**
     * Setup the configuration page
     *
     * @param string $baseurl
     */
    public function setup_page($baseurl){
        global $PAGE;

        // handles login, siteconfig requirement check, and page setup
        admin_externalpage_setup('report_canvasmigration_configuration');

        $this->pageurl = new \moodle_url($baseurl);
        $this->renderer = $PAGE->get_renderer('report_canvasmigration');

    }

    public function handle_request(){
        global $DB;

        switch($this->action){


            default:

                // first get the current config if there is any, and move to an array keyed by scoringitem
                $configs = $DB->get_records('report_canvasmigrationconfig');
                $scores = array();
                foreach($configs as $config){
                    $scores[$config->scoringitem] = $config->score;
                }

                $mform = new configscores($this->pageurl, $scores);

                if($data = $mform->get_data()){

                    $activitymodules = \core_component::get_plugin_list('mod');
                    foreach($activitymodules as $modname => $amodule){

                        $record = new \stdClass();
                        $record->scoringitem = $modname;
                        $record->score = $data->$modname;

                        if($existingrec = $DB->get_record('report_canvasmigrationconfig', array('scoringitem'=>$modname))){
                            $record->id = $existingrec->id;
                            $DB->update_record('report_canvasmigrationconfig', $record, true);
                        }else{
                            $DB->insert_record('report_canvasmigrationconfig', $record);
                        }
                    }

                    // TODO: update for calculated-{shared|nonshared} types.
                    // I'm postponing this since Mike says passing 0 for the score is okay.

                    foreach ( \question_bank::get_creatable_qtypes() as $qtypename => $qtype) {

                        $record = new \stdClass();
                        $record->scoringitem = $qtypename;
                        $record->score = $data->$qtypename;

                        if($existingrec = $DB->get_record('report_canvasmigrationconfig', array('scoringitem'=>$qtypename))){
                            $record->id = $existingrec->id;
                            $DB->update_record('report_canvasmigrationconfig', $record, true);
                        }else{
                            $DB->insert_record('report_canvasmigrationconfig', $record);
                        }

                    }

                    $this->renderer->setMessage('success', 'Success updated scores');
                }
                $this->renderer->config_header();
                $this->renderer->render_cofigure_page($mform);
                $this->renderer->config_footer();

                break;
        }

    }


}