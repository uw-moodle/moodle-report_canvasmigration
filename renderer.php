<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir . '/questionlib.php');

/**
 * canvas migration renderer
 *
 * @package     report_canvasmigration
 * @author      John Hoopes <hoopes@wisc.edu>
 * @copyright   2015 University of Wisconsin - Madison
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class report_canvasmigration_renderer extends plugin_renderer_base {

    /**
     * Sets a page message to display when the page is loaded into view
     *
     * base_header() must be called for the message to appear
     *
     * @param string $type
     * @param string $message
     */
    public function setMessage($type, $message) {
        $this->pageMessage = array($type, $message);
    }

    /**
     * Shows the message to the page.  is called by index_header
     */
    protected function showMessage(){

        if(empty($this->pageMessage)){
            return;
        }

        if (!is_array($this->pageMessage)) {
            return; // return if it's not an array
        }
        switch ($this->pageMessage[0]) {
            case 'error':
                echo $this->output->notification($this->pageMessage[1], 'notifiyproblem');
                break;
            case 'success':
                echo $this->output->notification($this->pageMessage[1], 'notifysuccess');
                break;
            case 'info':
                echo $this->output->notification($this->pageMessage[1], 'notifyinfo');
                break;
            default:
                // unrecognized notification type
                break;
        }
    }


    /**
     * show the header of the page, with message
     *
     */
    public function config_header(){
        echo $this->output->header();
        $this->showMessage();
    }

    /**
     * Output the footer of the page
     */
    public function config_footer(){
        echo $this->output->footer();
    }


    /**
     * Render the configuration page
     *
     * @param \report_canvasmigration\local\forms\configscores $form
     * @throws coding_exception
     */
    function render_cofigure_page($form){

        echo \html_writer::tag('h2', get_string('canvas_migration_config', 'report_canvasmigration'));

        $scoresurl = new moodle_url('https://docs.google.com/a/wisc.edu/spreadsheets/d/13rPsq5kfFDJvvFxVDonMwO3KVWFTj8abSYZtlGrJdfM/edit?usp=sharing');
        $scoreslink = html_writer::link($scoresurl, get_string('config_scores_link', 'report_canvasmigration'));
        echo \html_writer::div(get_string('configure_description', 'report_canvasmigration') . '<br />' . $scoreslink, 'canvasmigration_config_desc');

        $form->display();
    }

    public function index_header(){
        echo $this->output->header();
        $this->showMessage();

        echo '<p>This is an old version of this plugin.  The underlying code needs to be updated in order for the interface facing view works</p>';
    }

    public function index_footer(){
        echo $this->output->footer();
    }

    public function render_course_report($coursemods, $totalscore, $course){

        $this->page->requires->js('/report/canvasmigration/js/index.js');

        echo "the total score is: " . $totalscore;

        echo html_writer::start_div('canvasmigration');
        foreach($coursemods as $coursemod){

            echo $this->activitymodule_section($coursemod->id, ucfirst($coursemod->modname) . ' - ' . $coursemod->modinstance->name, $coursemod->score);
        }
        echo html_writer::end_div();

    }

    public function render_site_report($allcourses, $pageurl){


        $sitetable = new report_canvasmigration\local\tables\sitecoursereport('sitecoursereport');

        $sitetable->baseurl = $pageurl;
        $sitetable->setup();
        $sitetable->set_data($allcourses);


        $sitetable->show_download_buttons_at(array(TABLE_P_BOTTOM, TABLE_P_TOP));

        $sitetable->finish_output();

    }

    /**
     *
     * Print each row of data as a csv row
     *
     * @param array $data an array of arrays or objects
     */
    public function print_csv($data){

        foreach($data as $row){

            // if it's an object loop through to make an array for implode
            if(is_object($row)){
                $newrow = array();
                foreach($row as $value){
                    $newrow[] = $value;
                }
                $row = $newrow;
            }
            // do text formatting for better CSV formatting
            foreach($row as $index => $column){
                str_replace('"', '""', $column);
                if(is_string($column)){
                    $row[$index] = '"' . $column . '"';
                }
            }

            echo implode(',', $row) . "\n";
        }

    }

    /**
     * Builds an activity module
     *
     * @param string $id The id attribute to attach to the panel
     * @return string HTML string of the category section
     */
    protected function activitymodule_section($id, $name, $content){

        $catcontent = '';

        $catcontent .= \html_writer::start_div('panel panel-category', array('id'=>'panel_' . $id));

        $catcontent .= \html_writer::start_div('panel-heading');
        $pixicon = new \pix_icon('t/collapsed', 'open', 'moodle', array('class' => 'expandicon',
            'id' => 'panel_' . $id . '_expandicon'));
        $catcontent .= \html_writer::span($this->render($pixicon) . $name, 'panel-title');
        $catcontent .= \html_writer::end_div();

        $catcontent .= \html_writer::start_div('panel-body hidden');
        $catcontent .= $content;
        $catcontent .= \html_writer::end_div();

        $catcontent .= \html_writer::end_div();

        return $catcontent;
    }


}